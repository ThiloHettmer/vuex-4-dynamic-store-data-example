const state = () => ({
    components: []
})

const getters = {
    getComponentAttributeById: (state) => (id, attribute) => {
        const res = state.components.find(component => component.id === id);
        for (const key in res) {
            if (key === attribute) {
                return res[key];
            }
        }
    }
}

const actions = {}

const mutations = {
    addComponent (state, data) {
        state.components.push(data);
    },
    updateText (state, data) {
        const objIndex = state.components.findIndex((obj => obj.id === data.id));

        if (state.components[objIndex]) {
            state.components[objIndex].text = data.text;
        }
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
