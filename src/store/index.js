import { createStore } from 'vuex';
import dynamicComponentData from './modules/dynamicComponentData'

export default createStore({
    modules: {
        dynamicComponentData
    }
})
